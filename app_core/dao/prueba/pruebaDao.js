var Models = require("../../models/index");
var sequelize = Models.sequelize;
var Q = require("q");

var findUsuario = () => {
  return Models.AppUsuario.findAll({});
};

var prueba = () => {
  return { respues: "holaMundo" };
};

var findUsuarioById = (id) => {
  return Models.AppUsuario.findAll({
    include: [
      {
        model: Models.AppCuenta,
      },
    ],
    where: {
      id_usuario: id,
    },
  });
};

var findUsuarioTotalById = (id) => {
  return Models.AppUsuario.findAll({
    include: [
      {
        model: Models.AppCuenta,
        include: [
          {
            model: Models.AppMovimiento,
          },
        ],
      },
    ],
    where: {
      id_usuario: id,
    },
  });
};

var findCuenta = () => {
  return Models.AppCuenta.findAll({});
};

var createCuenta = (data) => {
  return Models.AppCuenta.create(data);
};

var findCuentaById = (id) => {
  return Models.AppCuenta.find({
    where: {
      id_cuenta: id,
    },
  });
};

var findMovimiento = () => {
  return Models.AppMovimiento.findAll({});
};

var findCuentaMovimiento = () => {
  return Models.AppCuenta.findAll({
    include: [
      {
        model: Models.AppMovimiento,
      },
    ],
  });
};

var findMovimientoByCuenta = (id) => {
  return Models.AppMovimiento.findAll({
    where: {
      id_cuenta: id,
    },
  });
};

var findMovimientoById = (id) => {
  return Models.AppMovimiento.findAll({
    where: {
      id_movimiento: id,
    },
  });
};

var findUsuarioCuentaMovimiento = () => {
  return Models.AppUsuario.findAll({
    include: [
      {
        model: Models.AppCuenta,
        include: [
          {
            model: Models.AppMovimiento,
          },
        ],
      },
    ],
  });
};
//-------------------------------------------------------------------------------------------------------------------------------

module.exports.findUsuario = findUsuario;
module.exports.prueba = prueba;
module.exports.findUsuarioById = findUsuarioById;
module.exports.findUsuarioTotalById = findUsuarioTotalById;
module.exports.findCuenta = findCuenta;
module.exports.createCuenta = createCuenta;
module.exports.findCuentaById = findCuentaById;
module.exports.findMovimiento = findMovimiento;
module.exports.findMovimientoByCuenta = findMovimientoByCuenta;
module.exports.findMovimientoById = findMovimientoById;
module.exports.findCuentaMovimiento = findCuentaMovimiento;
module.exports.findUsuarioCuentaMovimiento = findUsuarioCuentaMovimiento;
