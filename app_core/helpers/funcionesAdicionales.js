var padnum= function (num, size) {
  var s = num+''
  while (s.length < size) s = '0' + s
  return s
}

var formatearFecha= function (fechaparam, hora) {
  if(fechaparam) {
    var fecha= new Date(fechaparam)

    if(hora) {
      var mes= padnum(parseInt(fecha.getMonth())+1, 2)
      var dia= padnum(parseInt(fecha.getDate()), 2)
      var hora=padnum(parseInt(fecha.getHours()), 2)
      var minuto=padnum(parseInt(fecha.getMinutes()), 2)
      var cadena=dia+'/'+mes+'/'+ fecha.getFullYear()+' '+hora+':'+minuto
    }
    else{
      var mes= padnum(parseInt(fecha.getMonth())+1, 2)
      var dia= padnum(parseInt(fecha.getDate()), 2)
      var cadena=dia+'/'+mes+'/'+ fecha.getFullYear()
    }
    return cadena
  }
  else{
    return ''
  }
}

/**
* Funcion que retorna si el objeto ya esta en la lista
* @param {Array} arreglo - Lista en la cual se quiere buscar el objeto
* @param {Objet} identificador - atributo por el cual se quiere buscar
* @param {Objet} valor - vlaro que toma el atributo
* @returns {Function} buscarObjetoByKey - retorna objeto encontrado
**/
var buscarObjetoByKey= function (arreglo, identificador, valor) {
  var objetoEncontrado={}
  arreglo.forEach(function (item) {
    if(item[identificador]==valor) {
      objetoEncontrado=item
      return
    }
  })
  return objetoEncontrado
}

/**
* Funcion que retorna el nombre de la persona concatenado
* @param {Objet} persona - Objeto persona a la cual se quiere concatenar el nombre
* @returns {Function} concatenarNombre - retorna el nombre completo de la persona
**/
var concatenarNombre = function (persona) {
  var nombre=''
  var segundoNom = persona.segundo_nombre != null ? persona.segundo_nombre : ''
  var segundoApe = persona.segundo_apellido != null ? persona.segundo_apellido : ''
  nombre = persona.primer_nombre+' '+segundoNom+' '+persona.primer_apellido+' '+segundoApe

  return nombre
}

module.exports.padnum=padnum
module.exports.buscarObjetoByKey=buscarObjetoByKey
module.exports.formatearFecha=formatearFecha
module.exports.concatenarNombre=concatenarNombre
