
/**
 * @file archivo que contiene el modulo de funciones para el envio de mensajes de texto
 * @name funcionesSMS.js
 * @author David Villota <david.villlota@udenar.edu.co>
 * @license UDENAR
 * @copyright 2019 Udenar
 **/
var Request = require("request");

/**
 * Funcion que permite enviar mensajes de texto a traves de una api del modulo general
 * @param {string} destinatario - numero del telefono al que se enviara (se debe antepones el codigo del pais)
 * @param {string} remitente - modulo desde el que se envia - para el registro en los logs
 * @param {string} texto - mensaje de texto que se enviara, no debe ser superior a 150 caracteres
 * @returns {Object} promise - promesa de resolucion de la funcion
 */
var enviarSMS= (destinatario,remitente, texto)=>{

    return new Promise((resolve,reject)=>{
        
        var data= {
            remitente:remitente,
            destinatario:destinatario,
            texto: texto
        };
        Request.post({
            url:process.env.GENERAL+"/sms/enviarSMS",
            form:data
        },
        (err,httpResponse,body)=>{
            if(err){
                reject(err);
            }
            else if(httpResponse.statusCode==200){
                resolve({"message":"Mensaje enviado"});
            }
            else{
                reject(body);
            }

        });
    });
};

module.exports={
    enviarSMS:enviarSMS
}