
/**
 * @file archivo que contiene el modulo de funciones para el envio de correos electronicos
 * @name funcionesEmail.js
 * @author David Villota <david.villlota@udenar.edu.co>
 * @license UDENAR
 * @copyright 2019 Udenar
 **/
var Request = require("request");

/**
 * Funcion que permite enviar correos a traves de una api del modulo general
 * @param {*} htmlCorreo - contenido del correo
 * @param {*} user_correo - usuario a traves del cual se enviara el correo
 * @param {*} pass_correo - contraseña para enviar los correos
 * @param {*} destino - destino al que se enviara
 * @param {*} asunto - asunto del correo 
 * @returns {Object} promise - promesa de resolucion de la funcion
 */
var enviarEmail= (htmlCorreo,user_correo, pass_correo,destino,asunto)=>{

    return new Promise((resolve,reject)=>{
        
        var data= {
            htmlCorreo:htmlCorreo,
            user_correo:user_correo,
            pass_correo:pass_correo,
            destino:destino,
            asunto:asunto
        };
        Request.post({
            url:process.env.GENERAL+"/email/enviarcorreo",
            form:data
        },
        (err,httpResponse,body)=>{
            if(err){
                reject(err);
            }
            else if(httpResponse.statusCode==200){
                resolve({"message":"Mensaje enviado"});
            }
            else{
                reject(body);
            }

        });
    });
};

module.exports={
    enviarEmail
}