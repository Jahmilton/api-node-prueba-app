/**
 * @file archivo que contiene el modulo de funciones varias de seguridad
 * @name funcionesSeguridad.js
 * @author David Villota <david.villlota@udenar.edu.co>
 * @license UDENAR
 * @copyright 2016 Udenar
 **/
var Request= require("request");
var Q= require("q");
var Respuesta = require("../helpers/respuesta")
/**
* Modulo que agrupa todas las funciones de seguridad de autenticacion de tokens y desencriptacion de la informacion
* @module FuncionesSeguridad
**/
/**
* funcion middleware que administra y valida la autenticacion a traves del token conectandose con el servidor ARGUS
* @param {Object} req - objeto de peticion.
* @param {Object} res - objeto de respuesta.
* @param {function} next - funcion next.
* @returns {function} next- funcion next para continuar con la ejecucion del codigo que llama al middleware
**/
var autorizacion= function(req,res,next){
  if (req.headers.authorization && req.headers.authorization.search('Bearer ') === 0) {
      var token = req.headers.authorization.split(' ')[1];
      var url_base = req.baseUrl;
      var url_completa= req.originalUrl;
      var metodo= req.method;
      var data= {
        token: token,
        url_base:url_base,
        url_completa:url_completa,
        metodo:metodo
      };
      //TODO
      //AQUI VOY ME FALTA ENVIAR AL SERVIDOR LA PETICION Y MIRAR LAS COSITAS
      //y AQUI QUEDE PARA OTRA VERSION
      Request.post(
        {
            url:process.env.ARGUS+"/validarToken",
            form:data
        },
        function (err, httpResponse, body){
            if(err){
                Respuesta.sendJsonResponse(res,500,{"error":"existe un error en la autenticacion de la sesion"});
            }
            else if(httpResponse.statusCode==200){
                return next();
            }
            else{
                Respuesta.sendJsonResponse(res,500,body);
            }
        }
    );
  }
  else{
      Respuesta.sendJsonResponse(res,500,{"error":"el usuario no se encuentra autenticado"});
  }
};
var darInfotoken= function(req){
  var deferred= Q.defer();
  if (req.headers.authorization && req.headers.authorization.search('Bearer ') === 0) {
      var token = req.headers.authorization.split(' ')[1];
      var data={
          token:token,
      };
      Request.post({
          url:process.env.ARGUS+"/infotoken",
          form:data
      },
      function(err,httpResponse,body){
          if(err){
              deferred.reject(err);
          }
          else if(httpResponse.statusCode==200){
              var respuesta = JSON.parse(body);
              deferred.resolve(respuesta);
          }
          else{
              deferred.reject({"error":"error en la obtencion de la informacion"});
          }
      });
  }
  else{
     deferred.reject({"error":"no hay token de autorizacion"});
  }
  return deferred.promise;
};
var darInfoUsuario = function(token){
    var deferred= Q.defer();
    var data={
        token:token,
    };
    Request.post(
        {
            url:process.env.ARGUS+"/darinfoUsuario",
            form:data,
            headers: {
                'Authorization': 'Bearer '+token
            }
        },
        function(err,httpResponse,body){
            if(err){
                deferred.reject(err);
            }
            else if(httpResponse.statusCode==200){
                var respuesta = JSON.parse(body);
                deferred.resolve(respuesta);
            }
            else{
                deferred.reject({"error":"error en la obtencion de la informacion"});
            }
        }
    );
    return deferred.promise;
};
// funcion que permite hacer la peticion para el cierre de sesion
var cerrarSesion= function(token){
    var deferred= Q.defer();
    var data={
        token:token,
    };
    Request.post(
        {
            url:process.env.ARGUS+"/logoutexterno",
            form:data,
            headers: {
                'Authorization': 'Bearer '+token
            }
        },
        function(err,httpResponse,body){
            if(err){
                deferred.reject(err);
            }
            else if(httpResponse.statusCode==200){
                var respuesta = JSON.parse(body);
                deferred.resolve(respuesta);
            }
            else{
                deferred.reject({"error":"error en el cierre de sesion"});
            }
        }
    );
    return deferred.promise;
};
var darOpcionesModulo= function(token, modulo){
    var deferred= Q.defer();
    var data={
        token:token,
        modulo:modulo
    };
    Request.post(
        {
            url:process.env.ARGUS+"/daropcionesmodulo",
            form:data
        },
        function(err,httpResponse,body){
            if(err){
                deferred.reject(err);
            }
            else if(httpResponse.statusCode==200){
                var respuesta = JSON.parse(body);
                deferred.resolve(respuesta);
            }
            else{
                deferred.reject({"error":"error al obtener las opciones del usuario"});
            }
        }
    )
    return deferred.promise;
};
var autorizarRuta= function(token,ruta){
    var deferred= Q.defer();
    var data={
        token:token,
        ruta:ruta
    };
    console.log("aa",data);
    Request.post(
        {
            url:process.env.ARGUS+"/autorizarruta",
            form:data
        },
        function(err,httpResponse,body){
            if(err){
                deferred.reject(err);
            }
            else if(httpResponse.statusCode==200){
                var respuesta = JSON.parse(body);
                deferred.resolve(respuesta);
            }
            else{

                deferred.reject({"error":"error al verificar la autorizacion de la ruta"});
            }
        }
    )
    return deferred.promise;
};

var getOpciones = function(req){
    var deferred= Q.defer();
     var token = req.headers.authorization.split(' ')[1];
     // var ip= req.headers.ip_token;
      var data={
          token:token,
         // ip:ip
      };
    Request.post(
        {
            url:process.env.ARGUS+"/opciones",
            form:data,
            headers: {
                //'ip_token': ip,
                'Authorization': 'Bearer '+token
            }
        },
        function(err,httpResponse,body){
            if(err){
                deferred.reject(err);
            }
            else if(httpResponse.statusCode==200){
                var respuesta = JSON.parse(body);
                deferred.resolve(respuesta);
            }
            else{
                deferred.reject({"error":"error en la obtencion de la informacion"});
            }
        }
    );
    return deferred.promise;
};

var codificarIp=function(ip){
  var codificada=ip.replace(/\:/g,".");
  return codificada;
};
module.exports.autorizacion= autorizacion;
module.exports.darInfotoken= darInfotoken;
module.exports.darInfoUsuario=darInfoUsuario;
module.exports.cerrarSesion=cerrarSesion;
module.exports.darOpcionesModulo=darOpcionesModulo;
module.exports.autorizarRuta=autorizarRuta;
module.exports.getOpciones=getOpciones;