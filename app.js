//archivo de propiedades
require('dotenv').load();
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors= require("cors")

var routesPrueba= require('./app_prueba_api/routes/index');


var app = express();
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
//app.use(cors({origin: 'http://localhost:5005'}));
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '555mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/prueba', routesPrueba);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        if(err.name==="UnauthorizedError"){
          res.status(401);
          res.json({"message": err.name+":"+err.message });
        }
        else{
          res.status(err.status).json('error');
          res.status(err.status || 500);
          res.json('error', {
              message: err.message,
              error: err
          });
        }
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    console.log(err);
    res.json({"mensaje":"not found"});
});

Date.prototype.yyyymmdd = function() {

    return this.getFullYear() +	"" + ( this.getMonth() + 1 ) + "" + this.getDate() + "" + this.getHours() + "" + this.getMinutes() + "" + this.getSeconds();

};

console.log("servidor-------------------------------------------------");

module.exports = app;

//////* socket */////////
/*
var pg = require ('pg');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var pgConString = "postgres://postgres:pospru@90.1.1.32:5433/ciudenar"

pg.connect(pgConString, function(err, client) {

	if(err) {
		console.log(err);
	}

    client.on('notification', function(msg) {

		msg = JSON.parse( msg.payload );
		io.sockets.emit( msg.type, msg.data );

	});

	var query = client.query("LISTEN notitications");
});

io.on('connection', function(socket){

	console.log('a user connected');

});

http.listen(3000, function(){
	console.log('listening on *:3000');
});
*/
