var PruebaDao = require("../../app_core/dao/prueba/pruebaDao");
var Respuesta = require("../helpers/respuesta");

var findUsuario = (req, res) => {
  PruebaDao.findUsuario()
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var prueba = (req, res) => {
  console.log("ENTROOOO");
  Respuesta.sendJsonResponse(res, 200, {
    status: 200,
    response: "SELECT * FROM TABLE WHERE id_user = '" + req.params.id + "'",
  });
  /*PruebaDao.prueba()
    .then(function (respuesta) {
      console.log("ENTROOOO");
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });*/
};

var health = (req, res) => {
  Respuesta.sendJsonResponse(res, 200, {
    status: 200,
    response: "OK",
    port: "3001",
  });
};

var findUsuarioById = (req, res) => {
  PruebaDao.findUsuarioById(req.params.id)
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findUsuarioTotalById = (req, res) => {
  PruebaDao.findUsuarioTotalById(req.params.id)
    .then(function (respuesta) {
      let resultado = JSON.parse(JSON.stringify(respuesta));
      var egresos = 0;
      var ingresos = 0;
      for (let cuenta of resultado[0].AppCuenta) {
        if (cuenta.tipo == "E") {
          for (let mov of cuenta.AppMovimientos) {
            egresos = egresos + mov.valor;
          }
        }
        if (cuenta.tipo == "I") {
          for (let mov of cuenta.AppMovimientos) {
            ingresos = ingresos + mov.valor;
          }
        }
      }
      total = ingresos - egresos;

      Respuesta.sendJsonResponse(res, 200, { total: total });
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findCuenta = (req, res) => {
  PruebaDao.findCuenta()
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var createCuenta = (req, res) => {
  PruebaDao.createCuenta(req.body)
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findCuentaById = (req, res) => {
  PruebaDao.findCuentaById(req.params.id)
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findMovimiento = (req, res) => {
  PruebaDao.findMovimiento()
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findMovimientoByCuenta = (req, res) => {
  console.log("SOy un console-------------------------------------------");
  PruebaDao.findMovimientoByCuenta(req.params.id)
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findMovimientoById = (req, res) => {
  console.log("SOy un console-------------------------------------------");
  PruebaDao.findMovimientoById(req.params.id)
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findCuentaMovimiento = (req, res) => {
  PruebaDao.findCuentaMovimiento()
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

var findUsuarioCuentaMovimiento = (req, res) => {
  PruebaDao.findUsuarioCuentaMovimiento()
    .then(function (respuesta) {
      Respuesta.sendJsonResponse(res, 200, respuesta);
    })
    .catch(function (error) {
      Respuesta.sendJsonResponse(res, 200, {
        error: "se ha producido un error en la consulta",
      });
    });
};

//-------------------------------------------------------------------------------------------------------------------------------

module.exports.findUsuario = findUsuario;
module.exports.prueba = prueba;
module.exports.health = health;
module.exports.findUsuarioById = findUsuarioById;
module.exports.findUsuarioTotalById = findUsuarioTotalById;
module.exports.findCuenta = findCuenta;
module.exports.createCuenta = createCuenta;
module.exports.findCuentaById = findCuentaById;
module.exports.findMovimiento = findMovimiento;
module.exports.findMovimientoByCuenta = findMovimientoByCuenta;
module.exports.findMovimientoById = findMovimientoById;
module.exports.findCuentaMovimiento = findCuentaMovimiento;
module.exports.findUsuarioCuentaMovimiento = findUsuarioCuentaMovimiento;
