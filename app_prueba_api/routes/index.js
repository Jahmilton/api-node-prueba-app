var Express = require("express");
var Multipart = require("connect-multiparty");

var PruebaController = require("../controllers/pruebaController");

var router = Express.Router();
var multipartMiddleware = Multipart();

//-------------------------------------------------------------------------------------------------------------------------------

router.get("/findUsuario", PruebaController.findUsuario);
router.get("/user/:id", PruebaController.prueba);
router.get("/health", PruebaController.health);
router.get("/findUsuarioById/:id", PruebaController.findUsuarioById);
router.get("/findUsuarioTotalById/:id", PruebaController.findUsuarioTotalById);
router.get("/findCuenta", PruebaController.findCuenta);
router.post("/createCuenta", PruebaController.createCuenta);
router.get("/findCuentaById/:id", PruebaController.findCuentaById);
router.get("/findMovimiento", PruebaController.findMovimiento);
router.get("/findCuentaMovimiento", PruebaController.findCuentaMovimiento);
router.get(
  "/findMovimientoByCuenta/:id",
  PruebaController.findMovimientoByCuenta
);
router.get("/findMovimientoById/:id", PruebaController.findMovimientoById);
router.get(
  "/findUsuarioCuentaMovimiento",
  PruebaController.findUsuarioCuentaMovimiento
);

module.exports = router;
