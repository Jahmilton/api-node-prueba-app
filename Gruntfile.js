module.exports = function(grunt) {
    grunt.initConfig({
        jsdoc: {
            dist: {
 
                src: ['app_gestion_diplomas_actas_api/**/*.js', 'app_core/dao/gestion_diplomas_actas/**/*.js'],
                options: {
                    destination: 'docs/AppActasDiplomas',
                    ignoreWarnings: true
                }
            }
        },
    });
 
    grunt.loadNpmTasks('grunt-jsdoc');
 
    grunt.registerTask("default", ["jsdoc"]);
 };